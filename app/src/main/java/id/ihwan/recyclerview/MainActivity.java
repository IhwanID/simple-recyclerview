package id.ihwan.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private  RecyclerView recyclerView;
    private ArrayList<Karyawan>list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        list = new ArrayList<>();
        list.addAll(KaryawanData.getListData());

        showRecyclerList();
    }

    private void showRecyclerList(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RVAdapter listKaryawan = new RVAdapter(this);
        listKaryawan.setListKaryawan(list);
        recyclerView.setAdapter(listKaryawan);
    }
}
