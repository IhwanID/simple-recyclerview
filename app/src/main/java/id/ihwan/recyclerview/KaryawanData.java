package id.ihwan.recyclerview;

import java.util.ArrayList;

public class KaryawanData {

    public static String[][] data = new String[][]{
            {"Joko Narimo", "Kepala Divisi IT", "Cigasong", "http://i.pravatar.cc/150?img=4"},
            {"Andi Purnama", "HRD", "Jatitujuh", "http://i.pravatar.cc/150?img=7"},
            {"Aris Santoso", "Kepala Divisi Konsumsi", "Jatiwangi", "http://i.pravatar.cc/150?img=15"},
            {"Endi Rahmana", "Kepala Divisi Marketing", "Palasah", "http://i.pravatar.cc/150?img=11"},
            {"Doni Ramanto", "Admin Office", "Cikijing", "http://i.pravatar.cc/150?img=45"},
            {"Jodi Hermawan", "Pengurus Web", "Maja", "http://i.pravatar.cc/150?img=58"},
            {"Kurniadi Setiawan", "Marketing Manager", "Jatiwangi", "http://i.pravatar.cc/150?img=26"},
            {"Andrew Kurniadi", "Staff Office", "Jatiwangi", "http://i.pravatar.cc/150?img=36"},
            {"Benni Setiadi", "Marketing Manager", "Jatiwangi", "http://i.pravatar.cc/150?img=46"},
            {"Rahmat Awaludin", "Staff Office", "Palasah", "http://i.pravatar.cc/150?img=16"},
            {"Fitri Rahmawati", "Marketing Manager", "Kutamanggu", "http://i.pravatar.cc/150?img=45"},
            {"Erni Safinah", "Designer", "Jatiwangi", "http://i.pravatar.cc/150?img=70"},
            {"Adi Gunawan", "Marketing Manager", "Cikijing", "http://i.pravatar.cc/150?img=53"},
            {"Rofik Husni", "Staff Office", "Jatiwangi", "http://i.pravatar.cc/150?img=15"},
        };

    public static ArrayList<Karyawan> getListData(){

        Karyawan karyawan = null;

        ArrayList<Karyawan> list = new ArrayList<>();

        for (int i = 0; i <data.length; i++) {
            karyawan = new Karyawan();
            karyawan.setNama(data[i][0]);
            karyawan.setJabatan(data[i][1]);
            karyawan.setAsal(data[i][2]);
            karyawan.setGambar(data[i][3]);

            list.add(karyawan);
        }

        return list;
    }

}
